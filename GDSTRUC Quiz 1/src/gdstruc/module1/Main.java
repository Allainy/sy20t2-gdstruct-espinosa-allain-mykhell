package gdstruc.module1;

public class Main {

    public static void main(String[] args) {

        int[] numbers1 = {100, 32, 53, 6, -8, 21, 150, 225, 125, -25};
        int[] numbers2 = {125, 200, 150, 12, 15, 81, -24, 16, 89, -20};

        System.out.println("Descending Bubble Sort: \n");
        descendingBubble(numbers1);
        printingElements(numbers1);
        System.out.println("\n");
        System.out.println("Descending Selection Sort: \n");
        descendingSelection(numbers1);
        printingElements(numbers1);
        System.out.println("\n");
        System.out.println("Selection Sort (Smallest Value Searched):\n");
        modifiedSelection(numbers2);
        printingElements(numbers2);
    }
    private static void descendingBubble(int[] arr)
    {
        for (int lastIndex = arr.length -1; lastIndex > 0 ; lastIndex--)
        {
            for (int i = 0; i < lastIndex; i++)
            {
                if (arr[i] < arr[i + 1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i +1];
                    arr[i +1] = temp;
                }
            }
        }
    }
    private static void descendingSelection(int[] arr)
    {
        for (int lastIndex = arr.length -1; lastIndex > 0; lastIndex--)
        {
            int largestIndex = 0;

            for (int i = 1; i <= lastIndex; i++)
            {
                if (arr[i] < arr[largestIndex])
                {
                    largestIndex = i;
                }
            }
            int temp = arr[lastIndex];
            arr[lastIndex] = arr[largestIndex];
            arr[largestIndex] = temp;
        }
    }
    private static void modifiedSelection (int[] arr)
    {
        for (int lastIndex = arr.length -1; lastIndex > 0; lastIndex--)
        {
            int smallestIndex = 0;

            for (int i = 1; i <= lastIndex; i++)
            {
                if (arr[i] < arr[smallestIndex])
                {
                    smallestIndex = i;
                }
            }
            int temp = arr[lastIndex];
            arr[lastIndex] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }
    }
    private static void printingElements(int[] arr)
    {
        for (int j : arr)
        {
            System.out.print(j);
            System.out.print(" ");
        }
    }
}
