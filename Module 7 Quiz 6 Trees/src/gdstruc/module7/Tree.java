package gdstruc.module7;

public class Tree {

    private Node root;

    public void insert(int value)
    {
        if (root == null)
        {
            root = new Node(value);
        }
        else
        {
            root.insert(value);
        }
    }
    public void traversal()
    {
        if (root != null)
        {
            root.traversal();
        }
    }
    public void traverseInOrderDescending()
    {
        if (root != null)
        {
            root.traverseInOrderDescending();
        }
    }
    public Node get(int value)
    {
        if (root != null)
        {
            return root.get(value);
        }
        return null;
    }
    public void getMin()
    {
        int value = 0;
        if (root != null)
        {
            if (root.getLeftChild().getData() > root.getLeftChild().getLeftChild().getData())
            {
                value = root.getLeftChild().getLeftChild().getData();
            }
        }
        System.out.println("Min Value: " + value);
    }
    public void getMax()
    {
        int value = 0;
        if (root != null)
        {
            if (root.getRightChild().getData() > root.getRightChild().getLeftChild().getData())
            {
                value = root.getRightChild().getData();
            }
        }
        System.out.println("Max Value: " + value);
    }
}
