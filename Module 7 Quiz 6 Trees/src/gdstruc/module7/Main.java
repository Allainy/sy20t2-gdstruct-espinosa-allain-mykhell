package gdstruc.module7;

public class Main {

    public static void main(String[] args) {
        Tree tree = new Tree();

        tree.insert(10);
        tree.insert(7);
        tree.insert(16);
        tree.insert(5);
        tree.insert(15);
        tree.insert(6);
        tree.insert(13);

        System.out.println("In Order Descending: ");
        tree.traverseInOrderDescending();
        System.out.println();
        System.out.println("Getting Minimum Node: ");
        tree.getMin();
        System.out.println();
        System.out.println("Getting Maximum Node: ");
        tree.getMax();
    }
}
