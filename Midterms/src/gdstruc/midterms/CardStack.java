package gdstruc.midterms;

import java.util.EmptyStackException;

public class CardStack {
    private Card[] stack;
    private int top;
    private int count;

    public CardStack(int capacity)
    {
        stack = new Card[capacity];
    }

    public void push(Card deck)
    {
        if (top == stack.length)
        {
           Card[] newStack = new Card[2 * stack.length];
           System.arraycopy(stack, 0 ,newStack,0,stack.length);
           stack = newStack;
        }

        stack [top++]  = deck;
    }
    public Card pop()
    {
        if (isEmpty())
        {
            throw new EmptyStackException();
        }
        Card popCard = stack[--top];
        stack[top] = null;
        return popCard;
    }

    public Card peek()
    {
        if (isEmpty())
        {
            throw new EmptyStackException();
        }

        return stack[top -1];
    }

    public boolean isEmpty()
    {
        return stack == null;
    }

    public void print()
    {
        for (int i = top -1; i >= 0; i--)
        {
            System.out.print(stack[i] + " ");
        }
    }
}
