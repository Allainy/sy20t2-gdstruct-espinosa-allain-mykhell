package gdstruc.midterms;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Random rand = new Random();

        CardStack playerHand = new CardStack(30);
        CardStack playerDeck = new CardStack(30);
        CardStack discardPile = new CardStack(30);

        int size = 0;
        while (size != 10) {
            playerDeck.push(new Card("Gold"));
            playerDeck.push(new Card("Red"));
            playerDeck.push(new Card("Blue"));
            size++;
        }

        System.out.println("Game is starting!");
        while (!playerDeck.isEmpty())
        {
            int rand_int1 = rand.nextInt(3);
            if (rand_int1 == 0) {
                int rand_int2 = rand.nextInt(5);
                System.out.println("Draw " + (rand_int2 + 1) + " Cards from the stack");
                int a = 0;
                while(a != rand_int2 + 1) {
                        Card draw = playerDeck.pop();
                        playerHand.push(new Card(draw.getName()));
                        a++;
                    }
                }
            if (rand_int1 == 1) {
                int rand_int2 = rand.nextInt(5);
                System.out.println("Discard " + (rand_int2) + " Cards from your hand");
                int b = 0;
                while (b != rand_int2) {
                    if (playerHand == null) {
                        break;
                    }
                    else {
                        Card draw = playerHand.pop();
                        discardPile.push(new Card(draw.getName()));
                        b++;
                        }
                    }
            }  if (rand_int1 == 2) {
                int rand_int2 = rand.nextInt(5);
                System.out.println("Draw " + rand_int2 + " Cards from the Discard Pile");
                int c = 0;
                while (c != rand_int2) {
                    if (discardPile == null) {
                        break;
                    }
                    if (playerHand == null) {
                        break;
                    }
                    Card draw = discardPile.pop();
                    playerHand.push(new Card(draw.getName()));
                    c++;
                }
            }
            System.out.println("Your Hand is: ");
            playerHand.print();
            wait(1000);
            System.out.println();
        }
    }
    public static void wait(int ms)
    {
        try
        {
            Thread.sleep(ms);
        }
        catch (InterruptedException exception)
        {
            Thread.currentThread().interrupt();
        }
    }
}

