package gdstruc.module5;

public class Hashtable {
    private Player[] hashtable;

    public Hashtable()
    {
        hashtable = new Player [5];
    }

    private int hashKey(String key)
    {
        return key.length() % hashtable.length;
    }

    private boolean isOccupied(int index)
    {
        return hashtable[index] != null;
    }

    public void put(String key, Player value)
    {
        int hashedKey = hashKey(key);

        if (isOccupied(hashedKey))
        {
            int stoppingIndex = hashedKey;

            if (hashedKey == hashtable.length - 1)
            {
                hashedKey = 0;
            }
            else
            {
                hashedKey++;
            }

            while (isOccupied(hashedKey) && hashedKey != stoppingIndex)
            {
                hashedKey = (hashedKey - 1) & hashtable.length;
            }
        }
        if (hashtable[hashedKey] != null)
        {
            System.out.println("There is an element present at this index");
        }
        else
        {
            hashtable[hashedKey] = value;
        }
    }

    public Player get (String key)
    {
        int hashedKey = hashKey(key);
        return hashtable[hashedKey];
    }

    public void print()
    {
        for (int i = 0; i < hashtable.length; i++)
        {
            System.out.println("Element:" + i + " " + hashtable[i]);
        }
    }

    public void remove(String key)
    {
        int hashedKey = hashKey(key);
        if (hashtable[hashedKey] != null)
        {
            hashtable[hashedKey] = null;
        }
        else
        {
            System.out.println("There is no element to delete");
        }
    }
}
