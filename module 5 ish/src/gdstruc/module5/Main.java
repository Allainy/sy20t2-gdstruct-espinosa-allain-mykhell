package gdstruc.module5;

public class Main {

    public static void main(String[] args) {

        Player Joke = new Player (1, "Joke", 1);
        Player Ara = new Player (2, "Ara", 2);
        Player J = new Player(3,"J",3);
        Player Jessie = new Player(4, "Jessie",4);
        Player Do = new Player(5,"Do",5);

        Hashtable hashTable = new Hashtable();

        System.out.println("Initial hashtable");
        hashTable.put(Joke.getName(), Joke);
        hashTable.put(Ara.getName(), Ara);
        hashTable.put(J.getName(), J);
        hashTable.put(Jessie.getName(), Jessie);
        hashTable.put(Do.getName(), Do);

        hashTable.print();

        System.out.println();
        System.out.println("hashtable with removed element");
        hashTable.remove(Joke.getName());

        hashTable.print();
    }
}
