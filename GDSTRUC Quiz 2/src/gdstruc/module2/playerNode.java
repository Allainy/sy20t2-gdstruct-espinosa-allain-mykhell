package gdstruc.module2;

public class playerNode {
    private player player;
    private playerNode nextPlayer;

    @Override
    public String toString() {
        return "" + player;
    }

    public playerNode(player player) {
        this.player = player;
    }

    public gdstruc.module2.player getPlayer() {
        return player;
    }

    public void setPlayer(gdstruc.module2.player player) {
        this.player = player;
    }

    public playerNode getNextPlayer() {
        return nextPlayer;
    }

    public void setNextPlayer(playerNode nextPlayer) {
        this.nextPlayer = nextPlayer;
    }
}
