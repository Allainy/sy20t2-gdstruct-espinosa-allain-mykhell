package gdstruc.module2;

import java.util.LinkedList;

public class linkedList {
    private playerNode head;

    public void addToFront(player player)
    {
        playerNode playerNode = new playerNode(player);
        playerNode.setNextPlayer(head);
        head = playerNode;
    }

    public void printList()
    {
        playerNode current = head;
        System.out.print("Head -> ");
        while (current != null)
        {
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }

    public void remove()
    {
        playerNode current = head.getNextPlayer();
        System.out.print("Head ->");
        while (current != null) {
            System.out.print(current);
            System.out.print(" -> ");
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }

    public void count()
    {
        playerNode current = head;
        int counter = 0;
        while (current != null) {
            counter++;
            current = current.getNextPlayer();
        }
        System.out.println(counter);
    }

   public boolean contains(player name)
   {
       playerNode current = head;
      if (current.getPlayer() != name)
      {
        current = current.getNextPlayer();
      }
      if (current.getNextPlayer() == null)
      {
          System.out.println("false");
          return false;
      }
      else
      {
          System.out.println("true");
          return true;
      }
   }
   public void indexOf(player name) {
        playerNode current = head;
        while (current != null)
        {
            if (current.getPlayer() != name)
            {
                current = current.getNextPlayer();
            }
            else
            {
                System.out.print(current.getPlayer().getId());
                break;
            }
        }
   }
}
