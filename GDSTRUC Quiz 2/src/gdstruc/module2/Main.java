package gdstruc.module2;

public class Main {

    public static void main(String[] args) {
    player Ara = new player (1,"Ara", 1);
    player Boris = new player (2, "Boris", 2);
    player Catherine = new player (3, "Cathy", 3);

    linkedList playerLinkedList = new linkedList();

    playerLinkedList.addToFront(Ara);
    playerLinkedList.addToFront(Boris);
    playerLinkedList.addToFront(Catherine);

    System.out.println("Printed List: ");
    playerLinkedList.printList();
    System.out.println("List without the first element: ");
    playerLinkedList.remove();
    System.out.print("Number of elements in List: ");
    playerLinkedList.count();
    System.out.print("Does the List contain a player named Boris? ");
    playerLinkedList.contains(Boris);
    System.out.print("What is the index of Boris? ");
    playerLinkedList.indexOf(Boris);
    }
}
