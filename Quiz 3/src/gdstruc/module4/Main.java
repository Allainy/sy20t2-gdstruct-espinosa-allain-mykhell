package gdstruc.module4;

import java.util.ArrayList;
import java.util.Random;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        Scanner keyboard = new Scanner(System.in);
        Random rand = new Random();
        int turn = 1;
        ArrayList<number> playerList = new ArrayList(10);
        playerList.add(new number(1));
        playerList.add(new number(2));
        playerList.add(new number(3));
        playerList.add(new number(4));
        playerList.add(new number(5));
        playerList.add(new number(6));
        playerList.add(new number(7));
        playerList.add(new number(8));
        playerList.add(new number(9));
        playerList.add(new number(10));

        ArrayList<number> playerQueue = new ArrayList(10);
        ArrayList<number> successQueue = new ArrayList<>(10);

        while (turn != 10) {
            System.out.println("Turn: " + turn);
            System.out.println("Matchmaking in Progress");
            int random = rand.nextInt(7);
            wait(1000);
            Collections.shuffle(playerList);
            playerQueue.addAll(playerList);
            System.out.print("Press 'Enter' to see results");
            keyboard.nextLine();
            System.out.println("Players in Queue: ");
            for (int i = 0; i < random; i++)
            {
                System.out.print(playerQueue.get(i));
            }
            if (random >= 5) {
                successQueue.add(playerQueue.remove(0));
                successQueue.add(playerQueue.remove(0));
                successQueue.add(playerQueue.remove(0));
                successQueue.add(playerQueue.remove(0));
                successQueue.add(playerQueue.remove(0));
                System.out.println();
                System.out.println("Match Found with the following players:");
                for (int x = 0; x < 5; x++)
                {
                    System.out.print(successQueue.get(x));
                }
                System.out.println();
                System.out.println();
                wait(1000);
                playerQueue.clear();
                successQueue.clear();
                turn++;
            }
            else
            {
                System.out.println();
                System.out.println("Queue Failed! Insufficient Players!");
                System.out.println("Enter Queue Again?");
                keyboard.nextLine();
                playerQueue.clear();
                successQueue.clear();
            }
        }
    }
    public static void wait(int ms)
    {
        try
        {
            Thread.sleep(ms);
        }
        catch (InterruptedException exception)
        {
            Thread.currentThread().interrupt();
        }
    }
}
