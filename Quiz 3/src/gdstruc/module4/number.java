package gdstruc.module4;

import java.util.Objects;

public class number {
    private int number;

    public number (int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Player " + number + ", ";
    }
}
